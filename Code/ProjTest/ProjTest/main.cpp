#include <iostream>
#include <string>
#include "classData.h"
#include "dataGet.h"
#include "CSVWriter.h"
using namespace std;

void userInterface();	// uses the user's input to call member functions
char prompt();			// convert input words to single chars for switch menu
void mySpace();			// tab function
void help();			// list commands
void test();			// initialize display and delete clas objs
void testHub();			// safe data testing zone
void testHelp();		// list commands for testHub
char promptTestHub();	// "prompt" command for testHub

int main() {
	userInterface();
	return 0;
}


// This fuction calls prompt to get and format user input
// then uses the formatted input to call appropriate member functions
void userInterface() {
	Data myData;
	ifstream inFile;
	int fileCount = 0;
	string fileCountStr = "0";
	bool loop = 1;
	int rowCount = 0;
	char userResp;
	cout << "Welcome to the Assignment Group Project.\nTo get started, please enter a command.\n";
	while (loop == 1){
		userResp = prompt(); // grab and format user input
		switch (userResp) {
			case 'a':
				cout << "\"apple\" test confirmed.\n"; // tests the menu system
				break;
			case 'h':
				help();
				break;
			case 't':
				test();
				break;
			case 'z':
				cout << "Error: Invalid input. \nPlease type \"help\" for command options\n";
				break;
			case 'q':
				loop = 0;
				break;
			case 'w':
				testHub();
				break;
			case 'g':
				dataPull(myData);
				for (Data::outcome* current = myData.head; current != nullptr; current = current->next) {
					rowCount++;
				}
				inFile.open("RawCSV-" + fileCountStr + ".csv");
				while (inFile) {
					inFile.close();
					fileCount++;
					fileCountStr = to_string(fileCount);
					inFile.open("RawCSV-" + fileCountStr + ".csv");
				}
				inFile.close();
				if (myData.headStudent != nullptr) {
					myData.rawCSVOutput((rowCount + 1), fileCountStr);
					myData.averageCSVOutput(rowCount, fileCountStr);
					myData.percentCSVOutput((rowCount + 1), fileCountStr);
				}
				//rawWriter.writeToFile("RawCSV-" + fileCountStr + ".csv");
				//averageWriter.writeToFile("AverageCSV-" + fileCountStr + ".csv");
				//percentWriter.writeToFile("PercentCSV" + fileCountStr + ".csv");
				rowCount = 0;
				myData.clear();
				break;
			case 'v':
				verboseToggle();
				break;
		}

	}
}

// list all available commands
void help() {
	cout << "\n\"apple\" - apple test (simple input test)\n";	// tests the menu system
	cout << "\"help\" - display help message\n";
	cout << "\"testProtocol\" - linked list data testing\n";
	cout << "\"testHub\" - data entry testing platform\n";
	cout << "\"get data\" - pull data from outside source\n";
	cout << "\"verbose\" - toggles verbose mode (displays debug info)\n";
	cout << "\"quit\" - end program\n\n";
}

// creates, initalizes, displays, and deletes
// class object testData  
void test() {
	cout << "Initializing test...\n";
	Data testData;
	cout << "Initializing Outcome...\n";
	testData.addOutcome(1, "Test Outcome Alpha", 5, 4);
	testData.addOutcome(2, "Test Outcome Beta", 5 ,4);
	testData.addOutcome(500, "Test Outcome Zeta", 5, 4);
	cout << "Initializing Student...\n";
	testData.addStudent(1);
	cout << "Initializing Scores...\n";
	testData.giveScore(1, 1, 3.4);
	testData.giveScore(1, 2, 3.6);
	testData.giveScore(1, 500, 2.9);
	cout << "Displaying Outcome...\n\n\n";
	testData.checkOutcome();
	cout << "\n\n\n";
	cout << "Displaying Student and Scores...\n\n\n";
	testData.checkStudent();
	cout << "\n\n\n";
	cout << "Deleting data... \n";
}

// recieve user input and return appropriate 
// switch variable value
char prompt() {
	string userInput;
	cout << "Input:";
	mySpace();
	getline(cin, userInput);
	if (userInput == "apple" || userInput == "a") {
		return 'a';
	}
	else if (userInput == "help" || userInput == "h"){
		return 'h';
	}
	else if (userInput == "testProtocol" || userInput == "t") {
		return 't';
	}
	else if (userInput == "testHub" || userInput == "h") {
		return 'w';
	}
	else if (userInput == "quit" || userInput == "q") {
		return 'q';
	}
	else if (userInput == "get data" || userInput == "g") {
		return 'g';
	}
	else if (userInput == "verbose" || userInput == "v") {
		return 'v';
	}

	return 'z';	// default value
}

// custom tab function
void mySpace() {
	cout << "     ";
}

char promptTestHub() {
	string userInput;
	cout << ">>";
	mySpace();
	getline(cin, userInput);
	if (userInput == "exit") {
		cout << "\nexiting Test Hub\n";
		return 'e';
	}
	else if (userInput == "addStudent") {
		return 'a';
	}
	else if (userInput == "addOutcome") {
		return 'b';
	}
	else if (userInput == "addScore") {
		return 'c';
	}
	else if (userInput == "checkStudent") {
		return 'f';
	}
	else if (userInput == "checkOutcome") {
		return 'g';
	}
	else if (userInput == "help") {
		return 'h';
	}
	return 'z';
}

void testHub() {
	Data testData;
	bool loop = 1;
	char userResp = ' ';
	cout << "\nWelcome to the data entry testing platform\n";
	cout << "type \"help\" for a list of commands\n";
	//below are the variables used in holding data
	//Student Data
	//student placeholder number; iterates every time addStudent is run.
	int num = -1;
	//Outcome Data
	double id;
	string title;
	//Score Data
	int stuID;
	double outcomeID;
	double inScore;
	while (userResp != 'e') {
		userResp = promptTestHub();
		switch (userResp) {
		case 'a':
			//addStudent
			num++;
			testData.addStudent(num);
			cout << "\nstudent #" << num << " added\n\n";
			break;
		case 'b':
			//addOutcome
			cout << "Please enter the ID for the outcome:";
			mySpace();
			cin >> id;
			cout << "Please enter the title for the outcome:";
			mySpace();
			cin.ignore();
			getline(cin, title);
			testData.addOutcome(id, title, 5, 4);
			cout << "\nOutcome #" << id << ": " << title << " added\n";
			break;
		case 'c':
			//addScore
			cout << "Please enter the ID of the student you want to give a score:";
			mySpace();
			cin >> stuID;
			cout << "Please enter the ID of the outcome the score relates to:";
			mySpace();
			cin >> outcomeID;
			cout << "Please enter the score you wish to give the student:";
			mySpace();
			cin >> inScore;
			testData.giveScore(stuID, outcomeID, inScore);
			cout << "\nScore added\n";
			cin.ignore();
			break;
		case 'e':
			loop = 0;
			break;
		case 'f':
			testData.checkStudent();
			break;
		case'g':
			testData.checkOutcome();
			break;
		case 'h':
			testHelp();
			break;
		default:
			cout << "Invalid input, type \"help\" for available commands\n";
			break;
		}
	}
}

void testHelp() {
	std::cout << "\"addStudent\" - adds student\n";
	std::cout << "\"addOutcome\" - adds outcome\n";
	std::cout << "\"addScore\" - adds score to a student\n";
	std::cout << "\"checkStudent\" - checks the student list\n";
	std::cout << "\"checkOutcome\" - checks the outcome list\n";
	std::cout << "\"help\" - displays this message\n";
	std::cout << "\"exit\" - returns to base UI; deletes all lists\n";
	//chekstudent
	//checkoutcome
	//help
}