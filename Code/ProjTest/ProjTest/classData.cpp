#include "classData.h"
#include "CSVWriter.h"

int Data::addOutcome(int num, string title, int pointsPossible, int masteryPoints) {
	//newOutcome creates a new pointer to store the data
	outcome * newOutcome = new outcome;
	newOutcome->next = nullptr;
	newOutcome->id = num;
	newOutcome->title = title;
	newOutcome->masteryPoints = masteryPoints;
	newOutcome->pointsPossible = pointsPossible;
	//the if/else block finds the end of the linked list and appends the data in
	//newOutcome to the end of it
	if (head == nullptr) {
		head = newOutcome;
	}
	else {
		//the "current" pointer finds the end of the list, and sets it's "next" pointer
		//to the location of the new outcome node
		outcome* current = head;
		while (current->next != nullptr) {
			current = current->next;
		}
		current->next = newOutcome;
	}
	return num;
}

void Data::giveThreshhold(int temp, string description, int points) {
	if (head != nullptr) {
		for (outcome* search = head; search != nullptr; search = search->next) {
			if (search->id == temp) {
				search->addThreshhold(description, points);
				break;
			}
		}
	}
}

void Data::outcome::addThreshhold(string description, int points){
	threshhold* newThreshhold = new threshhold;
	newThreshhold->next = nullptr;
	newThreshhold->description = description;
	newThreshhold->points = points;
	if (threshholdList == nullptr) {
		threshholdList = newThreshhold;
	}
	else {
		threshhold* current = threshholdList;
		while (current->next != nullptr) {
			current = current->next;
		}
		current->next = newThreshhold;
	}
}

void Data::addStudent(int num) {
	//newStudent creates a new pointer to store the data
	students * newStudent = new students;
	newStudent->next = nullptr;
	newStudent->num = num;
	//the if/else block finds the end of the linked list and appends the data in
	//newOutcome to the end of it
	if (headStudent == nullptr) {
		headStudent = newStudent;
	}
	else {
		//the "current" pointer finds the end of the list, and sets it's "next" pointer
		//to the location of the new student node
		students* current = headStudent;
		while (current->next != nullptr) {
			current = current->next;
		}
		current->next = newStudent;
	}
}

void Data::giveScore(int stuID, int id, double inScore) {
	//the search pointer is used to check the IDs of the nodes in the list
	students* search = headStudent;
	//if the process ran successfully and the student was found, the bool will be set to 1
	//if it failed to locate the student, an error message will show
	bool success = 0;
	//this will search through the entire linked list for the student, stopping once it
	//finds them, or it reaches the end of the list
	for (; search != nullptr; search = search->next) {
		//if the search pointer matches the ID of the student we're looking for, it will run the command
		//to add a score to their "profile"
		if (search->num == stuID) {
			search->addScore(id, inScore);
			success = 1;
			break;
		}
	}
	if (success == 0) {
		cout << "Error, could not find student.";
	}
}

void Data::students::addScore(int id, double inScore) {
	//this pointer will hold the data that will be placed at the end of the linked list
	score * newScore = new score;
	newScore->next = nullptr;
	newScore->outcomeID = id;
	newScore->myScore = inScore;
	if (studentScore == nullptr) {
		studentScore = newScore;
	}
	else {
		score* current = studentScore;
		while (current->next != nullptr) {
			current = current->next;
		}
		current->next = newScore;
	}
}

void Data::checkOutcome() {
	//the current pointer will move through the linked list to show the data stored in each node
	outcome* current = head;
	if (current == nullptr){
		cout << "\nEmpty Outcomes list\n";
	}
	while (current != nullptr) {
		cout << "Outcome title:   " << current->title << endl;
		cout << "Outcome ID:   " << current->id << endl;
		cout << "Mastery points:   " << current->masteryPoints << endl;
		cout << "Points possible:   " << current->pointsPossible << endl;
		cout << endl;
		current = current->next;
	}
}

void Data::checkStudent() {
	//the current pointer will move through the linked list to show the data stored in each node
	students* current = headStudent;
	if (current == nullptr){
		cout << "\nEmpty Student list\n";
	}
	while (current != nullptr) {
		cout << "Student number:   " << current->num << endl;
		current->checkScore(current);
		cout << endl;
		current = current->next;
	}
}

void Data::students::checkScore(students* currentStudent) {
	//the current pointer will move through the linked list to show the data stored in each node
	score* current = currentStudent->studentScore;
	while (current != nullptr) {
		cout << "Outcome ID:   " << current->outcomeID << endl; 
		cout << "Score:   " << current->myScore << endl;
		cout << endl;
		current = current->next;
	}
}

void Data::readOutcomeFromStream() {
	double id;
	string title;
	for (outcome* current = head; current != nullptr; current = current->next) {
		id = current->id;
		title = current->title;
		//yourFunctionCall(id, title);
	}
}

void Data::readOutcomeFromStreamCSV(CSVWriter &writer) {
	//This function will run through the full list of outcomes and output to the CSV writer
	double id;
	string title;
	for (outcome* current = head; current != nullptr; current = current->next) {
		id = current->id;
		title = current->title;
		writer << title;
	}
}

void Data::readStudentFromStream() {
	int num;
	for (students* current = headStudent; current != nullptr; current = current->next) {
		num = current->num;
		//yourFunctionCall(num);
		current->readScoreFromStream(current);
	}
}

void Data::readStudentFromStreamCSV(CSVWriter &writer) {
	//This function outputs the students and their scores and outputs to the CSV writer
	int num;
	for (students* current = headStudent; current != nullptr; current = current->next) {
		num = current->num;
		writer << num;
		current->readScoreFromStreamCSV(current, writer);
	}
}

void Data::students::readScoreFromStream(students* currentStudent) {
	double myScore;
	double outcomeID;
	for (score* current = currentStudent->studentScore; current != nullptr; current = current->next) {
		current->myScore;
		current->outcomeID;
		//yourFunctionCall(myScore, outcomeID);
	}
}

void Data::students::readScoreFromStreamCSV(students* currentStudent, CSVWriter &writer) {
	//This function outputs the scores from a student to the CSV writer
	double myScore = 0;
	double outcomeID;
	for (score* current = currentStudent->studentScore; current != nullptr; current = current->next) {
		myScore = current->myScore;
		outcomeID = current->outcomeID;
		writer << myScore;
	}
}

void Data::rawCSVOutput(int rowCount, string fileCountStr) {
	CSVWriter writer;
	writer.enableAutoNewRow(rowCount);
	writer << "Students";
	//This function performs two functions, outputting the output titles and the students
	
	//This section will run through the scores of the head student (student 0) and use that to sort the outcome names to match how the scores are organized and output them to the CSV writer.
	for (students::score* current = headStudent->studentScore; current != nullptr; current = current->next) {
		for (outcome* currentOutcome = head; currentOutcome != nullptr; currentOutcome = currentOutcome->next) {
			if (current->outcomeID == currentOutcome->id) {
				writer << currentOutcome->title;
			}
		}
	}
	//This section runs through the students and their scores and outputs to the CSV writer
	for (students* current = headStudent; current != nullptr; current = current->next) {
		writer << current->num;
		current->readScoreFromStreamCSV(current, writer);
	}
	writer.writeToFile("RawCSV-" + fileCountStr + ".csv");
}

void Data::averageCSVOutput(int rowCount, string fileCountStr) {
	CSVWriter writer;
	writer.enableAutoNewRow(rowCount);

	double averageScore = 0;
	int outcomeCount = 0;
	int studentCount = 0;
	int count = 0;
	int averageCount = 0;
	for (outcome* current = head; current != nullptr; current = current->next) {
		outcomeCount++;
	}
	for (students* current = headStudent; current != nullptr; current = current->next) {
		studentCount++;
	}
	
	//Grab all scores from students per outcome and create average score.
	//Use count up procedure to circumvent arrangement issues
		//Write to row 1 in numerical order
		//Write to following rows in numerical order
	for (int i = 0; i < outcomeCount;) {
		outcome * current = head;
		while (current != nullptr) {
			if (count == current->id) {
				writer << current->title;
				i++;
			}
			current = current->next;
		}
		count++;
	}
	count = 0;
	for (int p = 0; p < (studentCount * outcomeCount);) {
		outcome* current = head;
		while (current != nullptr) {
			if (current->id == count) {
				students* testStudent = headStudent;
				while (testStudent != nullptr) {
					students::score* testScore = testStudent->studentScore;
					while (testScore != nullptr) {
						if (count == testScore->outcomeID) {
							averageScore = averageScore + testScore->myScore;
							averageCount++;
							p++;
						}
						testScore = testScore->next;
					}
					testStudent = testStudent->next;
				}
				averageScore = averageScore / averageCount;
				writer << averageScore;
			}
			current = current->next;
		}
		averageCount = 0;
		averageScore = 0;
		count++;
	}
	writer.writeToFile("AverageCSV-" + fileCountStr + ".csv");
}

void Data::percentCSVOutput(int rowCount, string fileCountStr) {
	CSVWriter writer;
	writer.enableAutoNewRow(rowCount);
	writer << "Rankings";
	for (students::score* current = headStudent->studentScore; current != nullptr; current = current->next) {
		for (outcome* currentOutcome = head; currentOutcome != nullptr; currentOutcome = currentOutcome->next) {
			if (current->outcomeID == currentOutcome->id) {
				writer << currentOutcome->title;
			}
		}
	}
	//Add to the class variables for mastery points, points possible, and thresholds.
	int count = 0;
	int outcomeCount = 0;
	int studentCount = 0;
	for (outcome* current = head; current != nullptr; current = current->next) {
		outcomeCount++;
	}
	for (students* current = headStudent; current != nullptr; current = current->next) {
		studentCount++;
	}
	bool firstThreshhold = 1;
	for (outcome* current = head; current != nullptr;) {
		if (current->id == count) {
			for (students* currentStudent = headStudent; currentStudent != nullptr; currentStudent = currentStudent->next) {
				for (students::score* currentScore = currentStudent->studentScore; currentScore != nullptr; currentScore = currentScore->next) {
					if (currentScore->outcomeID == current->id) {
						for (outcome::threshhold* currentThreshhold = current->threshholdList; currentThreshhold != nullptr; currentThreshhold = currentThreshhold->next) {
							if (firstThreshhold == 1 && currentScore->myScore == currentThreshhold->points) {
								currentThreshhold->count++;
								break;
							}
							else {
								firstThreshhold = 0;
							}
							if (currentScore->myScore < currentThreshhold->points && currentThreshhold->next == nullptr) {
								currentThreshhold->count++;
								break;
							}
							else if (currentScore->myScore < currentThreshhold->points && currentScore->myScore >= currentThreshhold->next->points) {
								currentThreshhold->count++;
								break;
							}
						}
					}
				}
			}
			current = current->next;
			count = 0;
		}
		count++;
	}
	//Percentage calculation
	//For development and testing purposes, it will instead be read as "count/total" instead of a percentage
	//The one critical flaw with this system is that it assumes that ALL outcomes share the same threshholds.
	bool descPrint = 1;
	int threshholdCount = 0;
	for (outcome::threshhold* current = head->threshholdList; current != nullptr; current = current->next) {
		threshholdCount++;
	}
	int threshholdLevel = 0;
	for (; threshholdLevel < threshholdCount; threshholdLevel++) {
		outcome::threshhold* test = head->threshholdList;
		for (int i = 0; i < threshholdLevel; i++) {
			test = test->next;
		}
		writer << test->description;
		for (outcome* current = head; current != nullptr; current = current->next) {
			outcome::threshhold* out = current->threshholdList;
			for (int o = 0; o < threshholdLevel; o++) {
				out = out->next;
			}
			writer << to_string(out->count) + "/" + to_string(studentCount);
		}
	}
	//Missing work tier
	int checkCount = 0;
	writer << "Missing Work";
	for (outcome* current = head; current != nullptr; current = current->next) {
		for (outcome::threshhold* currentThreshhold = current->threshholdList; currentThreshhold != nullptr; currentThreshhold = currentThreshhold->next) {
			checkCount = checkCount + currentThreshhold->count;
		}
		writer << to_string(checkCount - studentCount) + "/" + to_string(studentCount);
		checkCount = 0;
	}
	writer.writeToFile("PercentCSV-" + fileCountStr + ".csv");
}

Data::Data() {
	//Initializing the head of the outcome and student lists to nullptr
	head = nullptr;
	headStudent = nullptr;
}

Data::~Data() {
	//nodePtr is used to move the node used for deleting forward after the data from each node has been deleted.
	//listNode nodePtr
	outcome* outcomePtr = head;
	students* studentPtr = headStudent;
	/*
	*While delNode (The pointer I'll use for deleting) isn't a null pointer, this will continually run down the link list,
	*pulling the delNode to the nodePtr which is heading up the list until it gets to the nullptr at the end of the list.
	*/
	//cout << "Deleting all lists...\n";
	for (outcome* delNode = head; delNode != nullptr;) {
		outcomePtr = delNode->next;
		//cout << "Deleting node: 0x" << delNode;
		//cout << ", id: " << delNode->id;
		//cout << ", title:" << delNode->title;
		//cout << ", next: 0x" << delNode->next << endl;
		delNode->next = nullptr;
		delNode->id = NULL;
		delNode->title = "";
		delNode->masteryPoints = NULL;
		delNode->pointsPossible = NULL;
		delNode->deleteThreshhold(delNode->threshholdList);
		delete delNode;
		delNode = outcomePtr;
	}
	for (students* delNode = headStudent; delNode != nullptr;) {
		studentPtr = delNode->next;
		//cout << "Deleting node: 0x" << delNode;
		//cout << ", num: " << delNode->num;
		//cout << ", score:";
		delNode->deleteScore(delNode->studentScore);
		//cout << ", next: 0x" << delNode->next << endl;
		delNode->next = nullptr;
		delNode->num = NULL;
		delete delNode;
		delNode = studentPtr;
	}
	head = nullptr;
	headStudent = nullptr;
	
}

void Data::clear() {
	//nodePtr is used to move the node used for deleting forward after the data from each node has been deleted.
	//listNode nodePtr
	outcome* outcomePtr = head;
	students* studentPtr = headStudent;
	/*
	*While delNode (The pointer I'll use for deleting) isn't a null pointer, this will continually run down the link list,
	*pulling the delNode to the nodePtr which is heading up the list until it gets to the nullptr at the end of the list.
	*/
	//cout << "Deleting all lists...\n";
	for (outcome* delNode = head; delNode != nullptr;) {
		outcomePtr = delNode->next;
		//cout << "Deleting node: 0x" << delNode;
		//cout << ", id: " << delNode->id;
		//cout << ", title:" << delNode->title;
		//cout << ", next: 0x" << delNode->next << endl;
		delNode->next = nullptr;
		delNode->id = NULL;
		delNode->title = "";
		delNode->masteryPoints = NULL;
		delNode->pointsPossible = NULL;
		delNode->deleteThreshhold(delNode->threshholdList);
		delete delNode;
		delNode = outcomePtr;
	}
	for (students* delNode = headStudent; delNode != nullptr;) {
		studentPtr = delNode->next;
		//cout << "Deleting node: 0x" << delNode;
		//cout << ", num: " << delNode->num;
		//cout << ", score:";
		delNode->deleteScore(delNode->studentScore);
		//cout << ", next: 0x" << delNode->next << endl;
		delNode->next = nullptr;
		delNode->num = NULL;
		delete delNode;
		delNode = studentPtr;
	}
	head = nullptr;
	headStudent = nullptr;
}

void Data::outcome::deleteThreshhold(threshhold* threshholdList) {
	for (threshhold* delNode = threshholdList; delNode != nullptr;) {
		threshholdList = delNode->next;
		delNode->next = nullptr;
		delNode->count = NULL;
		delNode->description = "";
		delNode->points = NULL;
		delete delNode;
		delNode = threshholdList;
	}
}

void Data::students::deleteScore(score* studentScore) {
	for (score* delNode = studentScore; delNode != nullptr;) {
		studentScore = delNode->next;
		/*cout << "Deleting node: 0x" << delNode;
		cout << ", Outcome ID: " << delNode->myScore;
		cout << ", score:" << delNode->myScore;
		cout << ", next: 0x" << delNode->next << endl;*/
		delNode->next = nullptr;
		delNode->outcomeID = NULL;
		delNode->myScore = NULL;
		delete delNode;
		delNode = studentScore;
	}
}


//The purpose of this functions is to return the desired score to put format into a csv file
double Data::calulateScore(int i, int j){
	students* current = headStudent;
	double score{};
	for (int a = 0; i >= a; a++){
		score = current->getScore(current, j);
		current = current->next;
	}
	return score;
}

//The purpose of this function is to send score data to be calculated within function calculateScore
double Data::students::getScore(students* currentStudent, int i){
	score* current = currentStudent->studentScore;
	for (int a = 0; i > a; a++){
		current = current->next;
	}
	return current->myScore;
}

//The purpose of this functions is to return the desired outcome to put format into a csv file
string Data::calulateOutcome(int i){
	outcome* current = head;
	for (int a = 0; i > a; a++) {
		current = current->next;
	}
	return current->title;
}

