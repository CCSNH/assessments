#include <iostream>
#include <string>
#include "json/json.h"
#include "curl/curl.h"
#include <cstdint>
#include <memory>
#include <sstream>
#include "dataGet.h"
#include "classData.h"
using namespace std;

bool VERBOSE_MODE = 0;

string authQuery();
double convert(string outcomeID);
void verboseToggle();

namespace
{
	std::size_t callback(
		const char* in,
		std::size_t size,
		std::size_t num,
		char* out)
	{
		std::string data(in, (std::size_t) size * num);
		*((std::stringstream*) out) << data;
		return size * num;
	}
}

void dataPull(Data &myData) {
	string authKey = authQuery();
	//const string url("https://ccsnh.instructure.com/api/v1/courses/46291/outcome_rollups?rating_percents=true&per_page=20&include[]=outcomes&include[]=users&include[]=outcome_paths&page=1&sort_by=student&access_token=" + authKey);
	//const string url("https://ccsnh.instructure.com/api/v1/courses/46291/outcome_rollups?aggregate=course&include[]=outcome_paths&aggregate_stat=mean&access_token=" + authKey);
	const string url("https://ccsnh.instructure.com/api/v1/courses/46291/outcome_rollups?include[]=outcomes&include[]=users&include[]=outcome_paths&access_token=" + authKey);
	CURL* curl = curl_easy_init();

	// Set remote URL.
	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());

	// Don't bother trying IPv6, which would increase DNS resolution time.
	curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);

	// Don't wait forever, time out after 10 seconds.
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);

	// Follow HTTP redirects if necessary.
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

	// Response information.
	int httpCode(0);
	// std::unique_ptr<std::string> httpData(new std::string());

	std::stringstream httpData;

	// Hook up data handling function.
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callback);

	// Hook up data container (will be passed as the last parameter to the
	// callback handling function).  Can be any pointer type, since it will
	// internally be passed as a void pointer.
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &httpData);

	// Run our HTTP GET command, capture the HTTP response code, and clean up.
	curl_easy_perform(curl);
	curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
	curl_easy_cleanup(curl);
	if (httpCode == 200) {
		//Successful access
		cout << "Successful web access! Retrieving data...\n";
		// Response looks good - done using Curl now.  Try to parse the results
		// and print them out.
		Json::Value jsonData;
		Json::CharReaderBuilder jsonReader;
		std::string errs;

		if (Json::parseFromStream(jsonReader, httpData, &jsonData, &errs)) {
			// jsonReader.parse(httpData, jsonData))
			if (VERBOSE_MODE == 1) {
				std::cout << "Successfully parsed JSON data" << std::endl;
				std::cout << "\nJSON data received:" << std::endl;
				std::cout << jsonData.toStyledString() << std::endl;
			}
			double idDouble;
			double scoreDouble;
			for (int i = 0; 1 != jsonData["rollups"][i].isNull();) {
				myData.addStudent(i);
				for (int o = 0; 1 != jsonData["rollups"][i]["scores"][o].isNull();) {
					idDouble = convert(jsonData["rollups"][i]["scores"][o]["links"]["outcome"].asString());
					scoreDouble = jsonData["rollups"][i]["scores"][o]["score"].asDouble();
					myData.giveScore(i, idDouble, scoreDouble);
					o++;
				}
				i++;
			}
			int temp = 0;
			for (int i = 0; 1 != jsonData["linked"]["outcome_paths"][i].isNull();) {
				temp = myData.addOutcome(jsonData["linked"]["outcome_paths"][i]["id"].asInt(), jsonData["linked"]["outcome_paths"][i]["parts"][0]["name"].asString(), jsonData["linked"]["outcomes"][i]["points_possible"].asInt(), jsonData["linked"]["outcomes"][i]["mastery_points"].asInt());
				for (int p = 0; 1 != jsonData["linked"]["outcomes"][i]["ratings"][p].isNull();) {
					myData.giveThreshhold(temp, jsonData["linked"]["outcomes"][i]["ratings"][p]["description"].asCString(), jsonData["linked"]["outcomes"][i]["ratings"][p]["points"].asInt());
					p++;
				}
				i++;
			}

			myData.checkStudent();
			myData.checkOutcome();

			std::cout << std::endl;
		}
		else {
			std::cout << "Could not parse HTTP data as JSON" << std::endl;
			std::cout << "HTTP data was:\n" << httpData.str() << std::endl;
			return;
		}
	}
	else if (httpCode == 401) {
		//Bad access
		cout << "Error: Access key invalid.\n";
	}
	else {
		cout << "Error: Unknown httpCode.\n";
	}
}

double convert(string outcomeID) {
	ostringstream stream;
	double conversion = 1;
	string test;
	bool loop = 1;
	while (loop == 1){
		stream << conversion;
		test = stream.str();
		if (test != outcomeID) {
			conversion++;
			stream.str("");
			stream.clear();
		}
		else if (test == outcomeID){
			loop = 0;
		}
		else if (conversion > 10000) {
			//ERROR
		}
	}

	return conversion;
}

string authQuery() {
	string authKey;
	cout << "Please enter your authentication key:     ";
	getline(cin, authKey);
	return authKey;
}

void verboseToggle() {
	if (VERBOSE_MODE == 0) {
		cout << "Verbose mode enabled\n";
		VERBOSE_MODE = 1;
	}
	else if (VERBOSE_MODE == 1) {
		cout << "Verbose mode disabled\n";
		VERBOSE_MODE = 0;
	}
}

/*
#define CURL_STATICLIB
#include <cstdint>
#include <iostream>
#include <memory>
#include <string>
#include <sstream>

#include "curl/curl.h"
#include "json/json.h"
#include "classData.h"

namespace
{
	std::size_t callback(
		const char* in,
		std::size_t size,
		std::size_t num,
		char* out)
	{
		std::string data(in, (std::size_t) size * num);
		*((std::stringstream*) out) << data;
		return size * num;
	}
}

int main()
{
	//const std::string url("http://date.jsontest.com/");
	//const std::string url("https://canvas.instructure.com/api/v1/courses");
	const std::string url("https://ccsnh.instructure.com/api/v1/courses/46291/outcome_rollups?rating_percents=true&per_page=20&include[]=outcomes&include[]=users&include[]=outcome_paths&page=1&sort_by=student&access_token=9466~TzZUmaJQUZwpe54RDMcMYaIQo1eWwuzQRKv3LfYuSBJLzfmbiCbLPzz3ijCuI1x3");
	CURL* curl = curl_easy_init();

	// Set remote URL.
	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());

	// Don't bother trying IPv6, which would increase DNS resolution time.
	curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);

	// Don't wait forever, time out after 10 seconds.
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);

	// Follow HTTP redirects if necessary.
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

	// Response information.
	int httpCode(0);
	// std::unique_ptr<std::string> httpData(new std::string());

	std::stringstream httpData;

	// Hook up data handling function.
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callback);

	// Hook up data container (will be passed as the last parameter to the
	// callback handling function).  Can be any pointer type, since it will
	// internally be passed as a void pointer.
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &httpData);

	// Run our HTTP GET command, capture the HTTP response code, and clean up.
	curl_easy_perform(curl);
	curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
	curl_easy_cleanup(curl);
	
	if (httpCode == 200){
		std::cout << "\nGot successful response from " << url << std::endl;

		// Response looks good - done using Curl now.  Try to parse the results
		// and print them out.
		Json::Value jsonData;
		Json::CharReaderBuilder jsonReader;
		std::string errs;

		if (Json::parseFromStream(jsonReader, httpData, &jsonData, &errs)){
			// jsonReader.parse(httpData, jsonData))

			std::cout << "Successfully parsed JSON data" << std::endl;
			std::cout << "\nJSON data received:" << std::endl;
			//std::cout << jsonData.toStyledString() << std::endl;

			std::cout << jsonData["rollups"][0]["scores"][7];
			std::cout << std::endl;
		}
		else{
			std::cout << "Could not parse HTTP data as JSON" << std::endl;
			std::cout << "HTTP data was:\n" << httpData.str() << std::endl;
			return 1;
		}
	}
	
	if (httpCode == 401) {
		std::cout << "\nGot successful response from " << url << std::endl;

		// Response looks good - done using Curl now.  Try to parse the results
		// and print them out.
		Json::Value jsonData;
		Json::CharReaderBuilder jsonReader;
		std::string errs;

		if (Json::parseFromStream(jsonReader, httpData, &jsonData, &errs)) {
			// jsonReader.parse(httpData, jsonData))

			std::cout << "Successfully parsed JSON data" << std::endl;
			std::cout << "\nJSON data received:" << std::endl;
			std::cout << jsonData.toStyledString() << std::endl;

			std::cout << "Status: " << jsonData["status"] << std::endl;
			std::cout << "Error:" << jsonData["errors"] << std::endl;
			//const Json::Value& 
			std::cout << "Errors -> Message: " << jsonData["errors"][0]["message"].toStyledString() << std::endl;
		}
		else {
			std::cout << "Could not parse HTTP data as JSON" << std::endl;
			std::cout << "HTTP data was:\n" << httpData.str() << std::endl;
			return 1;
		}
	}
	else {
		std::cout << "Couldn't GET from " << url << " - exiting" << std::endl;
		return 1;
	}
	return 0;
}
*/