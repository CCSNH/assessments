#include <iostream>
#include <fstream>
#include <string>
#include "CSVWriter.h"
#include "IntList.h"
#include "time.h"

using namespace std;

//The purpose of this functions is to return the desired score to put format into a csv file
double Data::calulateScore(int i, int j) 
{
	
	students* current = headStudent;
	double score{};

	for (int a = 0; i >= a; a++) 
	{
		score = current->getScore(current, j);
		current = current->next;
	}
	
	return score;
}

//The purpose of this function is to send score data to be calculated within function calculateScore
double Data::students::getScore(students* currentStudent, int i) 
{
	score* current = currentStudent->studentScore;	
	
	for (int a = 0; i > a; a++) 
		current = current->next;
	
	return current->myScore;
}

//The purpose of this functions is to return the desired outcome to put format into a csv file
string Data::calulateOutcome(int i)
{
	outcome* current = head;
	
	for (int a = 0; i > a; a++) 
		current = current->next;
	

	return current->title;
}


