#pragma once
#include <iostream>
#include <string>
#include "classData.h"
#include "CSVWriter.h"
using namespace std;

class Data {
public:
	//Constructor and destructor for the class
	Data();
	~Data();

	//linked-list struct for creating outcomes, holds data of outcome ID, and outcome title
	struct outcome {		
		struct threshhold {
			string description;
			int points;
			int count = 0;
			threshhold* next;
		};
		string title;
		double id;
		outcome* next = nullptr;
		int pointsPossible;
		int masteryPoints;
		threshhold* threshholdList = nullptr;
		void addThreshhold(string description, int points);
		void deleteThreshhold(threshhold* threshholdList);
	};

	//linked-list struct for students, holds student id (num), and student's score
		//student's scores listed in score linked-list struct
			//score struct holds outcome ID, and the score of that student
	struct students {
		struct score {
			int outcomeID;
			double myScore;
			score* next;
		};
		int num;
		score* studentScore = nullptr;
		students* next = nullptr;
		void addScore(int id, double inScore);
		void checkScore(students* currentStudent);
		void deleteScore(score* studentScore);
		void readScoreFromStream(students* currentStudent);
		void readScoreFromStreamCSV(students* currentStudent, CSVWriter &writer);
		double getScore(students* currentStudent, int i);
	};
	//The head pointer for the outcome and student structs.
	outcome* head;
	students* headStudent;

	/*
	*List of functions:
		*addOutcome(<ID of outcome>, <title of outcome>)
			*adds an outcome to the linked list
		*addStudent(<ID of student>)
			*adds a student to the linked list
		*checkOutcome()
			*prints the data stored in the outcomes linked list
		*checkStudent()
			*prints the data stored in the students linked list
		*giveScore(<ID given to student>, <outcome ID the score relates to>, <points earned>)
			*adds a value to the student's score linked list
		*readOutcomeFromStreamCSV(<CSVWriter>)
			*outputs outcome data to the CSV writer
		*readStudentFromStreamCSV(<CSVWriter>)
			*outputs students data to the CSV writer
		*readScoreFromStreamCSV(<CSVWriter>)
			*outputs score data to the CSV writer
		*CSVOutput(<CSVWriter>)
			*outputs all class data to the CSV writer and sorts the outcome titles to match the scores
	*/
	int addOutcome(int num, string title, int pointsPossible, int masteryPoints);
	void addStudent(int num);
	void checkOutcome();
	void checkStudent();
	void giveScore(int stuID, int id, double inScore);
	void giveThreshhold(int id, string description, int points);
	void readOutcomeFromStream();
	void readOutcomeFromStreamCSV(CSVWriter &writer);
	void readStudentFromStream();
	void readStudentFromStreamCSV(CSVWriter &writer);
	void rawCSVOutput(int rowCount, string fileCountStr);
	void averageCSVOutput(int rowCount, string fileCountStr);
	void percentCSVOutput(int rowCount, string fileCountStr);
	void clear();

	double calulateScore(int i, int j);
	string calulateOutcome(int i);
};
