#pragma once
#include <iostream>
#include <string>
#include "IntList.h"
#include "CSVWriter.h"
using namespace std;

class Data {
public:
	//Constructor and destructor for the class
	Data();
	~Data();

	//linked-list struct for creating outcomes, holds data of outcome ID, and outcome title
	struct outcome {
		string title;
		double id;
		outcome* next = nullptr;
	};

	//linked-list struct for students, holds student id (num), and student's score
		//student's scores listed in score linked-list struct
			//score struct holds outcome ID, and the score of that student
	struct students {
		struct score {
			double outcomeID;
			double myScore;
			score* next;
		};
		int num;
		score* studentScore = nullptr;
		students* next = nullptr;
		void addScore(double id, double inScore);
		void checkScore(students* currentStudent);
		void deleteScore(score* studentScore);
		void readScoreFromStream(students* currentStudent);

		double getScore(students* currentStudent, int i);
	};
	//The head pointer for the outcome and student structs.
	outcome* head;
	students* headStudent;

	/*
	*List of functions:
		*addOutcome(<ID of outcome>, <title of outcome>)
			*adds an outcome to the linked list
		*addStudent(<ID of student>)
			*adds a student to the linked list
		*checkOutcome()
			*prints the data stored in the outcomes linked list
		*checkStudent()
			*prints the data stored in the students linked list
		*giveScore(<ID given to student>, <outcome ID the score relates to>, <points earned>)
			*adds a value to the student's score linked list
	*/
	void addOutcome(int num, string title);
	void addStudent(int num);
	void checkOutcome();
	void checkStudent();
	void giveScore(int stuID, double id, double inScore);
	void readOutcomeFromStream();
	void readStudentFromStream();

	double calulateScore(int i, int j);
	string calulateOutcome(int i);
};
