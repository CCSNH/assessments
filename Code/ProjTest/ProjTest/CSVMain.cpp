
#include <iostream>
#include <fstream>
#include <string>
#include "CSVWriter.h"
#include "IntList.h"
#include "time.h"
using namespace std;

int main()
{
	
	// Do "Press any key to continue..." on exit
	atexit([] {system("pause"); });
	
	int studNum;					//the number of students
	int outNum;						//the number of outcomes
	int score;						// student's random score
	srand(time(0));					//Gets random numbers
	const int studentID = 77832;	// Constant for Student ID
	const int outcomeID = 54321;	// Constant for Outcome ID
	string out;						//Name of Outcome
	Data class1;					//Initializiation of Data class
	CSVWriter operations;			//Initialization of CSVWriter Class
	
	cout << "How many students? >> ";
	cin >> studNum;
	
	cout << "How many outcomes? >> ";
	cin >> outNum;

	for (int i = 0; i < studNum; i++)
	{
		class1.addStudent(i + studentID);
	}
	
	for (int j = 0; j < outNum; j++)
	{
		for (int i = 0; i < studNum; i++)
		{
			score = rand() % 6;
			class1.giveScore(i + studentID, outcomeID + j, score);
		}
	}

	for (int i = 0; outNum > i; i++)
	{
		cout << "Please enter the name of the outcome you want to add: ";
		cin >> out;


		class1.addOutcome(i + outcomeID, out);

	}

	//Important: Everything avove this is code to generate a random set of data. All the calculations are below this and the functions are located in the CSV.cpp file.
	int studC; //Counter for student number
	int outC; //Counter for Outcome Number
	
	operations.enableAutoNewRow(outNum + 1); //Sets the max amount of rows in the file to the amount of outcomes + 1

	operations << "Students";	//First item in the CSV File always stays the same

	for (outC = 0; outNum > outC; outC++) //Saves the outcome name into the CSVWriter Class
		operations << class1.calulateOutcome(outC);
	

	for (studC = 0; studNum > studC; studC++) //Saves the student score into the CSVWriter Class. 
	{
		string student = "Student #" + to_string(studC + 1);
		operations << student;

		for (outC = 0; outNum > outC; outC++)
			operations << class1.calulateScore(studC, outC);
	}
	operations.writeToFile("Hello.csv"); //Writes the data to the CSV Document
}