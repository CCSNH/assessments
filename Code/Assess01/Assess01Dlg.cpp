// CSCI285N - Capstone Project - Assesssment Team
// File: Assess01Dlg.cpp
// Description:  : Implementation file for the Assess01Dlg class
// Author: Bill Allen
// Modified: 3-28-2020

// #include "pch.h"
#include "framework.h"
#include "Assess01.h"
#include "Assess01Dlg.h"
#include "afxdialogex.h"


#include <iostream>
#include <string>
#include <json/json.h>
#include <curl/curl.h>
#include <cstdint>
#include <memory>
#include <sstream>
#include <fstream>

#include "classData.h"
#include "convert.h"
#include "popup.h"
#include "CSVWriter.h"
// #include "dataGet.h"

using namespace std;


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

string dataPull(string myAuthorization, string myCourseNumber, Data &myData);
// Implementation for the About dialog. //////////////////////////////////

// CAboutDlg -	class definition for default About dialog
class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

// Constructor for the About dialog
CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

// Function to retrieve input from the About dialog
// if there was any to retrieve.
void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

// Message map to link dialog controls to event handlers
BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

// End of CAboutDlg implementation ////////////////////////////////


// Implementation for the CAssess01Dlg dialog /////////////////////

// Constructor for the CAssess01Dlg dialog
CAssess01Dlg::CAssess01Dlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_ASSESS01_DIALOG, pParent)
//	, m_authKey(_T(""))
//	, m_courseNum(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_authKey = _T("");
	m_courseNum = _T("");
}

// Destructor for the CAssess01Dlg dialog
CAssess01Dlg::~CAssess01Dlg()
{
}

// Function to retrieve input from the CAssess01Dlg dialog
void CAssess01Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
//	DDX_Text(pDX, IDC_EDIT_AUTH_KEY, m_authKey);
//	DDX_Text(pDX, IDC_EDIT_COURSE_NUM, m_courseNum);
}

// Message map links dialog controls to event handlers.
BEGIN_MESSAGE_MAP(CAssess01Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_GEN_REPORT, &CAssess01Dlg::OnBnClickedButtonGenReport)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

// CAssess01Dlg message/event handlers

BOOL CAssess01Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	// SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	// Add a background image.
	this->SetBackgroundImage(IDB_BITMAP2, BACKGR_TILE, TRUE);

	return TRUE;  // return TRUE  unless you set the focus to a control

} // End of OnInitDialog()

//////////////////////////////////////////////////////////////////////
// PreTranslateMessage() -	filters out the Enter and Cancel key
//							presses from keyboard input stream.
//
BOOL CAssess01Dlg::PreTranslateMessage(MSG *pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			pMsg->wParam = NULL;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);

} // End of PreTranslateMessage()

void CAssess01Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAssess01Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAssess01Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

////////////////////////////////////////////////////////////////////////////////
// OnBnClickedButtonGenReport() - event handler for the Generate Report button.
//
void CAssess01Dlg::OnBnClickedButtonGenReport()
{
	// Data myData;

	// Retrieve Authorization Key and Course Number from text boxes.
	CString authKeyText = _T("");
	int firstNameLength = this->GetDlgItemText(IDC_EDIT_AUTH_KEY, authKeyText);
	CString courseNumText = _T("");
	int courseNumLength = this->GetDlgItemText(IDC_EDIT_COURSE_NUM, courseNumText);

	if (firstNameLength + courseNumLength)
	{
		//Authorization key retrieval and conversion to string
		CT2CA pszConvertedAuthKeyText(authKeyText);
		string authKey(pszConvertedAuthKeyText);
		//Course number retrieval and conversion to string
		CT2CA pszConvertedCourseNumText(courseNumText);
		string courseNum(pszConvertedCourseNumText);
		//Data storage object
		Data myData;
		//Data gets stored in object, and returns string for error checking
		string text = dataPull(authKey, courseNum, myData);
		if (text == "httperror") {
			MessageBox("Could not parse HTTP data as JSON.\nCheck authorization key and course number.", "Error", MB_OK | MB_ICONERROR);
			return;
		}
		else if(text == "error") {
			MessageBox("Unknown error.\nCheck authorization key and course number.", "Error", MB_OK | MB_ICONERROR);
			return;
		}
		else if (text == "401") {
			MessageBox("Bad access.\nCheck authorization key and course number.", "Error", MB_OK | MB_ICONERROR);
			return;
		}
		else if (text == "???") {
			MessageBox("Unknown http code.\nCheck authorization key and course number.", "Error", MB_OK | MB_ICONERROR);
			return;
		}
		ifstream inFile;
		int fileCount = 0;
		string fileCountStr = "0";
		int rowCount = 0;
		//Counts the number of rows to be output from counting outcomes
		for (Data::outcome* current = myData.head; current != nullptr; current = current->next) {
			rowCount++;
		}
		//Checking for existing files to prevent overwrites
		inFile.open("RawCSV-" + fileCountStr + ".csv");
		while (inFile) {
			inFile.close();
			fileCount++;
			fileCountStr = to_string(fileCount);
			inFile.open("RawCSV-" + fileCountStr + ".csv");
		}
		inFile.close();
		//Printing to all three output files
		if (myData.headStudent != nullptr) {
			myData.rawCSVOutput((rowCount + 1), fileCountStr);
			myData.averageCSVOutput(rowCount, fileCountStr);
			myData.percentCSVOutput((rowCount + 1), fileCountStr, 1);
		}
		//Resetting rowCount to prevent growth among repeated requests
		rowCount = 0;
		//Clearing Data storage to prevent excess data
		myData.clear();
		MessageBox("The CSV files have been generated successfully.", "File Output", MB_OK | MB_ICONINFORMATION);
	}
	else
	{
		//Error message if no data was entered
		CString messageText(_T("No characters were retrieved"));
		CString myCaption(_T("Retrieval unsuccessful"));
		MessageBox(messageText, myCaption, MB_OK | MB_ICONERROR);
	}

} // End of OnBnClickedButtonGenReport()


/*
////////////////////////////////////////////////////////////////////////////////
// OnBnClickedButtonGenReport() - event handler for the Generate Report button.
//
void CAssess01Dlg::OnBnClickedButtonGenReport()
{
	// Retrieve Authorization Key and Course Number from text boxes.
	CString tempCString = _T("");

	// Call UpdateData(TRUE) to retrieve data from edit controls.
	UpdateData(TRUE);

	// Swap values around.
	tempCString = m_authKey;
	m_authKey = m_courseNum;
	m_courseNum = tempCString;

	// Call UpdateData(FALSE) to push values out to edit controls.
	UpdateData(FALSE);

} // End of OnBnClickedButtonGenReport()
*/


HBRUSH CAssess01Dlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  Change any attributes of the DC here
	switch (nCtlColor)
	{
	case CTLCOLOR_STATIC:
		pDC->SetTextColor(RGB(255, 255, 255));
		return (HBRUSH)GetStockObject(NULL_BRUSH);
	}

	// TODO:  Return a different brush if the default is not desired
	return hbr;
}

//callback and parts of dataPull obtained from https://stackoverflow.com/questions/47257198/parsin-issue-warning-with-json 
std::size_t callback(
	const char* in,
	std::size_t size,
	std::size_t num,
	char* out)
{
	std::string data(in, (std::size_t) size * num);
	*((std::stringstream*) out) << data;
	return size * num;
}

string dataPull(string myAuthorization, string myCourseNumber, Data &myData){
	//authKey is the authorization key
	//courseNum is the course number
	//url is the url used in the http call to get the json data from the Canvas API
	string authKey = myAuthorization;
	string courseNum = myCourseNumber;
	string url("https://ccsnh.instructure.com/api/v1/courses/" + courseNum + "/outcome_rollups?include[]=outcomes&include[]=users&include[]=outcome_paths&access_token=" + authKey);

	// Convert url back into a CString.
	CString convertedUrl(_T("ccsnh instructure com/api v1 courses"));
	
	// Compose message.
	CString myCaption(_T("Constructed URL:"));
	// MessageBox(convertedUrl, myCaption, MB_OK | MB_ICONINFORMATION);
	CURL* curl = curl_easy_init();

	// Set remote URL.
	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());

	// Don't bother trying IPv6, which would increase DNS resolution time.
	curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);

	// Don't wait forever, time out after 10 seconds.
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);

	// Follow HTTP redirects if necessary.
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

	// Response information.
	int httpCode(0);
	// std::unique_ptr<std::string> httpData(new std::string());

	std::stringstream httpData;

	// Hook up data handling function.
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callback);

	// Hook up data container (will be passed as the last parameter to the
	// callback handling function).  Can be any pointer type, since it will
	// internally be passed as a void pointer.
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &httpData);

	// Run our HTTP GET command, capture the HTTP response code, and clean up.
	curl_easy_perform(curl);
	curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
	curl_easy_cleanup(curl);
	if (httpCode == 200) {
		//Successful access
		cout << "Successful web access! Retrieving data...\n";
		// Response looks good - done using Curl now.  Try to parse the results
		// and print them out.
		Json::Value jsonData;
		Json::CharReaderBuilder jsonReader;
		std::string errs;

		if (Json::parseFromStream(jsonReader, httpData, &jsonData, &errs)) {
			//Json data output (for debugging)
			/*
			ofstream outFile;
			outFile.open("json.txt");
			outFile << "JSON data:  ";
			outFile << jsonData;
			outFile << "\n\n\n\n\nTest:  ";
			outFile << jsonData["rollups"][0]["scores"][0]["links"]["outcome"];
			outFile.close();
			*/
			
			//for loops that use JSON data will use .isNull from the Json object to check if there is data to be obtained
			
			//This for loop adds students to the Data object and stores data of each student's scores and "ids"
			for (int i = 0; 1 != jsonData["rollups"][i].isNull();) {
				myData.addStudent(i);
				for (int o = 0; 1 != jsonData["rollups"][i]["scores"][o].isNull();) {
					double idDouble = 0;
					double scoreDouble = 0;
					idDouble = convertJson(jsonData["rollups"][i]["scores"][o]["links"]["outcome"], 0);
					scoreDouble = jsonData["rollups"][i]["scores"][o]["score"].asDouble();
					myData.giveScore(i, idDouble, scoreDouble);
					o++;
				}
				i++;
			}
			//Adding outcomes to the Data object and storing their names, ids, and threshhold values
			int temp = 0;
			for (int i = 0; 1 != jsonData["linked"]["outcome_paths"][i].isNull();) {
				temp = myData.addOutcomeJson(jsonData["linked"]["outcome_paths"][i]["id"], jsonData["linked"]["outcome_paths"][i]["parts"][0]["name"], jsonData["linked"]["outcomes"][i]["points_possible"].asInt(), jsonData["linked"]["outcomes"][i]["mastery_points"].asInt());
				for (int p = 0; 1 != jsonData["linked"]["outcomes"][i]["ratings"][p].isNull();) {
					myData.giveThreshhold(temp, jsonData["linked"]["outcomes"][i]["ratings"][p]["description"].asCString(), jsonData["linked"]["outcomes"][i]["ratings"][p]["points"].asInt());
					p++;
				}
				i++;
			}
		}
		else {
			//Error: Could not parse HTTP data as JSON
			return "httperror";
		}
		return "success";
	}
	else if (httpCode == 401) {
		//Bad access
		//Error: Access key invalid.
		return "401";
	}
	else {
		//Error: Unknown httpCode.
		return "???";
	}
	//Error: Unknown error
	return "error";
}