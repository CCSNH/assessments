#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include "json/json.h"
#include "CSVWriter.h"
// #include "classData.h"

using namespace std;

class Data {
public:
	//Constructor and destructor for the class
	Data();
	~Data();

	//linked-list struct for creating outcomes. holds title, id, and threshholds, as well as points possible, and the amount of mastery points available
		//threshhold data held in the threshhold struct
			//linked-list struct for creating threshholds, holds description, points, and count of students
	struct outcome {
		struct threshhold {
			string description;
			int points;
			//count goes mostly unused, except in processing of the percent output CSV
			int count = 0;
			threshhold* next;
		};
		int pointsPossible;
		int masteryPoints;
		double id;
		string title;
		outcome* next = nullptr;
		threshhold* threshholdList = nullptr;
		void addThreshhold(string description, int points);
		void deleteThreshhold(threshhold* threshholdList);
	};

	//linked-list struct for students, holds student id (num), and student's score
		//student's scores listed in score linked-list struct
			//score struct holds outcome ID, and the score of that student
	struct students {
		struct score {
			int outcomeID;
			double myScore;
			score* next;
		};
		int num;
		score* studentScore = nullptr;
		students* next = nullptr;
		void addScore(double id, double inScore);
		void checkScore(students* currentStudent);
		void printScore(students* currentStudent, ofstream &outFile);
		void deleteScore(score* studentScore);
		void readScoreFromStreamCSV(students* currentStudent, CSVWriter &writer);
	};
	//The head pointer for the outcome and student structs.
	outcome* head;
	students* headStudent;

	/*
	*List of functions:
		*addOutcomeJson(<ID of outcome from Json object>, <title of outcome from Json object>, <points possible>, <mastery points available>)
			*adds an outcome to the linked list
		*addStudent(<ID of student>)
			*adds a student to the linked list
		*averageCSVOutput(<rowCount>, <CSVWriter>)
			*writes to a CSV file with averages of the data
		*checkOutcome()
			*prints the data stored in the outcomes linked list
		*checkStudent()
			*prints the data stored in the students linked list
		*clear()
			*deletes all data from the object
		*giveScore(<ID given to student>, <outcome ID the score relates to>, <points earned>)
			*adds a value to the student's score linked list
		*giveThreshhold(<id>, <description of threshhold>, <points for threshhold>)
			*adds a threshhold to the outcome linked list
		*percentCSVOutput(<rowCount>, <CSVWriter>)
			*writes to a CSV file with rates for threshholds
		*printOutcome()
			*prints outcome data via std::cout (currently only uses id and title)
		*printStudent()
			*prints student data via std::cout (outputs student id with score and outcome id)
		*rawCSVOutput(<rowCount>, <CSVWriter>)
			*outputs all class data to the CSV writer and sorts the outcome titles to match the scores
		*readOutcomeFromStreamCSV(<CSVWriter>)
			*outputs outcome data to the CSV writer
		*readStudentFromStreamCSV(<CSVWriter>)
			*outputs students data to the CSV writer
	*/
	int addOutcomeJson(Json::Value num, Json::Value title, int pointsPossible, int masteryPoints);
	void addStudent(int num);
	void averageCSVOutput(int rowCount, string fileCountStr);
	void checkOutcome();
	void checkStudent();
	void clear();
	void giveScore(int stuID, double id, double inScore);
	void giveThreshhold(int id, string description, int points);
	void percentCSVOutput(int rowCount, string fileCountStr, bool mode);
	void printOutcome();
	void printStudent();
	void rawCSVOutput(int rowCount, string fileCountStr);
	void readOutcomeFromStreamCSV(CSVWriter &writer);
	void readStudentFromStreamCSV(CSVWriter &writer);
};
