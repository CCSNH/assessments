#include "classData.h"
#include "CSVWriter.h"
#include "convert.h"

//Data storage functions; adds data to the Data object

int Data::addOutcomeJson(Json::Value num, Json::Value title, int pointsPossible, int masteryPoints) {
	//newOutcome creates a new pointer to store the data
	outcome * newOutcome = new outcome;
	newOutcome->next = nullptr;
	newOutcome->id = convertJson(num, 1);
	newOutcome->title = title.asCString();
	//newOutcome->pointsPossible = convertJson(pointsPossible, 0);
	newOutcome->pointsPossible = pointsPossible;
	//newOutcome->masteryPoints = convertJson(masteryPoints, 0);
	newOutcome->masteryPoints = masteryPoints;
	//the if/else block finds the end of the linked list and appends the data in
	//newOutcome to the end of it
	if (head == nullptr) {
		head = newOutcome;
	}
	else {
		//the "current" pointer finds the end of the list, and sets it's "next" pointer
		//to the location of the new outcome node
		outcome* current = head;
		while (current->next != nullptr) {
			current = current->next;
		}
		current->next = newOutcome;
	}
	return newOutcome->id;
}

void Data::addStudent(int num) {
	//newStudent creates a new pointer to store the data
	students * newStudent = new students;
	newStudent->next = nullptr;
	newStudent->num = num;
	//the if/else block finds the end of the linked list and appends the data in
	//newOutcome to the end of it
	if (headStudent == nullptr) {
		headStudent = newStudent;
	}
	else {
		//the "current" pointer finds the end of the list, and sets it's "next" pointer
		//to the location of the new student node
		students* current = headStudent;
		while (current->next != nullptr) {
			current = current->next;
		}
		current->next = newStudent;
	}
}

void Data::giveScore(int stuID, double id, double inScore) {
	//the search pointer is used to check the IDs of the nodes in the list
	students* search = headStudent;
	//if the process ran successfully and the student was found, the bool will be set to 1
	//if it failed to locate the student, an error message will show
	bool success = 0;
	//this will search through the entire linked list for the student, stopping once it
	//finds them, or it reaches the end of the list
	for (search != headStudent; search != nullptr; search = search->next) {
		//if the search pointer matches the ID of the student we're looking for, it will run the command
		//to add a score to their "profile"
		if (search->num == stuID) {
			search->addScore(id, inScore);
			success = 1;
			break;
		}
	}
	if (success == 0) {
		cout << "Error, could not find student.";
	}
}

void Data::students::addScore(double id, double inScore) {
	//this pointer will hold the data that will be placed at the end of the linked list
	score * newScore = new score;
	newScore->next = nullptr;
	newScore->outcomeID = id;
	newScore->myScore = inScore;
	if (studentScore == nullptr) {
		studentScore = newScore;
	}
	else {
		score* current = studentScore;
		while (current->next != nullptr) {
			current = current->next;
		}
		current->next = newScore;
	}
}

void Data::giveThreshhold(int temp, string description, int points) {
	//This function finds the outcome that uses the id from the threshhold and adds a threshhold's descriptionand points to the outcome
	if (head != nullptr) {
		for (outcome* search = head; search != nullptr; search = search->next) {
			if (search->id == temp) {
				search->addThreshhold(description, points);
				break;
			}
		}
	}
}

void Data::outcome::addThreshhold(string description, int points) {
	//This creates a place for the threshhold data to be stored and attaches it to the outcome
	threshhold* newThreshhold = new threshhold;
	newThreshhold->next = nullptr;
	newThreshhold->description = description;
	newThreshhold->points = points;
	if (threshholdList == nullptr) {
		threshholdList = newThreshhold;
	}
	else {
		threshhold* current = threshholdList;
		while (current->next != nullptr) {
			current = current->next;
		}
		current->next = newThreshhold;
	}
}

//Debugging commands; prints data from object out to files

void Data::checkOutcome() {
	//the current pointer will move through the linked list to show the data stored in each node
	outcome* current = head;
	if (current == nullptr)
	{
		cout << "\nEmpty Outcomes list\n";
	}
	while (current != nullptr) {
		cout << "Outcome title:   " << current->title << endl;
		cout << "Outcome ID:   " << current->id << endl;
		cout << endl;
		current = current->next;
	}
}


void Data::printOutcome() {
	//the current pointer will move through the linked list to show the data stored in each node
	outcome* current = head;
	ofstream outFile;
	outFile.open("outcomes.txt");
	if (current == nullptr)
	{
		outFile << "\nEmpty Outcomes list\n";
	}
	while (current != nullptr) {
		outFile << "Outcome title:   " << current->title << endl;
		outFile << "Outcome ID:   " << current->id << endl;
		outFile << endl;
		current = current->next;
	}
	outFile.close();
}

void Data::checkStudent() {
	//the current pointer will move through the linked list to show the data stored in each node
	students* current = headStudent;
	if (current == nullptr)
	{
		cout << "\nEmpty Student list\n";
	}
	while (current != nullptr) {
		cout << "Student number:   " << current->num << endl;
		current->checkScore(current);
		cout << endl;
		current = current->next;
	}
}

void Data::printStudent() {
	//the current pointer will move through the linked list to show the data stored in each node
	students* current = headStudent;
	ofstream outFile;
	outFile.open("students.txt");
	if (current == nullptr)
	{
		outFile << "\nEmpty Student list\n";
	}
	while (current != nullptr) {
		outFile << "Student number:   " << current->num << endl;
		current->printScore(current, outFile);
		outFile << endl;
		current = current->next;
	}
	outFile.close();
}

void Data::students::checkScore(students* currentStudent) {
	//the current pointer will move through the linked list to show the data stored in each node
	score* current = currentStudent->studentScore;
	while (current != nullptr) {
		cout << "Outcome ID:   " << current->outcomeID << endl; 
		cout << "Score:   " << current->myScore << endl;
		cout << endl;
		current = current->next;
	}
}

void Data::students::printScore(students* currentStudent, ofstream &outFile) {
	//the current pointer will move through the linked list to show the data stored in each node
	score* current = currentStudent->studentScore;
	while (current != nullptr) {
		outFile << "Outcome ID:   " << current->outcomeID << endl;
		outFile << "Score:   " << current->myScore << endl;
		outFile << endl;
		current = current->next;
	}
}

void Data::readOutcomeFromStreamCSV(CSVWriter &writer) {
	//This function will run through the full list of outcomes and output to the CSV writer
	double id;
	string title;
	for (outcome* current = head; current != nullptr; current = current->next) {
		id = current->id;
		title = current->title;
		writer << title;
	}
}

void Data::readStudentFromStreamCSV(CSVWriter &writer) {
	//This function outputs the students and their scores and outputs to the CSV writer
	int num;
	for (students* current = headStudent; current != nullptr; current = current->next) {
		num = current->num;
		writer << num;
		current->readScoreFromStreamCSV(current, writer);
	}
}

void Data::students::readScoreFromStreamCSV(students* currentStudent, CSVWriter &writer) {
	//This function outputs the scores from a student to the CSV writer
	double myScore = 0;
	double outcomeID;
	for (score* current = currentStudent->studentScore; current != nullptr; current = current->next) {
		myScore = current->myScore;
		outcomeID = current->outcomeID;
		writer << myScore;
	}
}

//CSV writer output functions

void Data::rawCSVOutput(int rowCount, string fileCountStr) {
	CSVWriter writer;
	writer.enableAutoNewRow(rowCount);
	writer << "Students";
	//This function performs two functions, outputting the output titles and the students

	//This section will run through the scores of the head student (student 0) and use that to sort the outcome names to match how the scores are organized and output them to the CSV writer.
	for (students::score* current = headStudent->studentScore; current != nullptr; current = current->next) {
		for (outcome* currentOutcome = head; currentOutcome != nullptr; currentOutcome = currentOutcome->next) {
			if (current->outcomeID == currentOutcome->id) {
				writer << currentOutcome->title;
			}
		}
	}
	//This section runs through the students and their scores and outputs to the CSV writer
	for (students* current = headStudent; current != nullptr; current = current->next) {
		writer << current->num;
		current->readScoreFromStreamCSV(current, writer);
	}
	writer.writeToFile("RawCSV-" + fileCountStr + ".csv");
}

void Data::averageCSVOutput(int rowCount, string fileCountStr) {
	//Initalizing the writer to be used
	CSVWriter writer;
	writer.enableAutoNewRow(rowCount);

	double averageScore = 0;
	int outcomeCount = 0;
	int studentCount = 0;
	int count = 0;
	int averageCount = 0;
	//Counts outcome and students to know how many outputs to use
	for (outcome* current = head; current != nullptr; current = current->next) {
		outcomeCount++;
	}
	for (students* current = headStudent; current != nullptr; current = current->next) {
		studentCount++;
	}

	//Grab all scores from students per outcome and create average score.
	//Count up using IDs to circumvent arrangement issues, allowing printings in numerical order
	
	//Grabs titles from outcomes and writes them to file
	//count checks id numbers and outcomeCount counts the number of outcomes have been written, and stops once all have been written
	for (int i = 0; i < outcomeCount;) {
		outcome * current = head;
		while (current != nullptr) {
			if (count == current->id) {
				writer << current->title;
				i++;
			}
			current = current->next;
		}
		count++;
	}
	count = 0;
	//Repeat until all students and outcomes are accounted for
	for (int p = 0; p < (studentCount * outcomeCount);) {
		outcome* current = head;
		while (current != nullptr) {
			//check outcomes until a matching id is found with count
			if (current->id == count) {
				//check all students
				students* testStudent = headStudent;
				while (testStudent != nullptr) {
					students::score* testScore = testStudent->studentScore;
					while (testScore != nullptr) {
						//for any scores that match outcome ID, iterate averageCount and add their score to averageScore
						//iterate "p" to track progress
						if (count == testScore->outcomeID) {
							averageScore = averageScore + testScore->myScore;
							averageCount++;
							p++;
						}
						testScore = testScore->next;
					}
					testStudent = testStudent->next;
				}
				//Create average score using (averageScore / averageCount)
				averageScore = averageScore / averageCount;
				writer << averageScore;
			}
			current = current->next;
		}
		//Reset count and score to prevent data from bleeding between outcomes
		averageCount = 0;
		averageScore = 0;
		count++;
	}
	writer.writeToFile("AverageCSV-" + fileCountStr + ".csv");
}

//The mode boolean sets how the output will be formatted
	//0 = students / totalStudents
	//1 = students%
void Data::percentCSVOutput(int rowCount, string fileCountStr, bool mode) {
	//Initializing the writer for the CSV
	CSVWriter writer;
	writer.enableAutoNewRow(rowCount);
	writer << "Rankings";
	for (students::score* current = headStudent->studentScore; current != nullptr; current = current->next) {
		for (outcome* currentOutcome = head; currentOutcome != nullptr; currentOutcome = currentOutcome->next) {
			if (current->outcomeID == currentOutcome->id) {
				writer << currentOutcome->title;
			}
		}
	}
	//Add to the class variables for mastery points, points possible, and thresholds.
	int count = 0;
	int outcomeCount = 0;
	int studentCount = 0;
	for (outcome* current = head; current != nullptr; current = current->next) {
		outcomeCount++;
	}
	for (students* current = headStudent; current != nullptr; current = current->next) {
		studentCount++;
	}
	//This section of the code focuses on counting how many students are in which threshholds
	//The firstThreshhold boolean tracks if this is the first threshhold of an outcome for if a student gets the best score
	bool firstThreshhold = 1;
	for (outcome* current = head; current != nullptr;) {
		//Search the outcomes until an id matches count
		if (current->id == count) {
			//Check all students
			for (students* currentStudent = headStudent; currentStudent != nullptr; currentStudent = currentStudent->next) {
				//Check all scores
				for (students::score* currentScore = currentStudent->studentScore; currentScore != nullptr; currentScore = currentScore->next) {
					//Check if the score matches the id
					//After if statement, reset firstThreshhold to 1, in case it was set to 0
					if (currentScore->outcomeID == current->id) {
						//Check all threshholds
						for (outcome::threshhold* currentThreshhold = current->threshholdList; currentThreshhold != nullptr; currentThreshhold = currentThreshhold->next) {
							//Check if the score matches the highest threshhold, otherwise, set firstThreshhold to 0
							double test = currentThreshhold->points;
							if (firstThreshhold == 1 && currentScore->myScore == test) {
								currentThreshhold->count++;
								//writer << currentScore->myScore;
								//break;
							}
							else {
								firstThreshhold = 0;
							}
							//Check if the score falls into the lowest threshhold next to nullptr
							if (currentScore->myScore < currentThreshhold->points && currentThreshhold->next == nullptr) {
								//currentThreshhold->count++;
								//break;
							}
							//Otherwise, check that it is higher than next and less than current
							else if (currentScore->myScore < test && currentScore->myScore >= currentThreshhold->next->points) {
								currentThreshhold->next->count++;
								//writer << currentScore->myScore;
								//break;
							}
						}
					}
					firstThreshhold = 1;
				}
			}
			current = current->next;
			count = 0;
		}
		count++;
	}
	//Percentage calculation
	//For development and testing purposes, it will instead be read as "count/total" instead of a percentage
	//The one critical flaw with this system is that it assumes that ALL outcomes share the same threshholds.
	bool descPrint = 1;
	int threshholdCount = 0;
	for (outcome::threshhold* current = head->threshholdList; current != nullptr; current = current->next) {
		threshholdCount++;
	}
	int threshholdLevel = 0;
	count = 0;
	//This part of the code prints the data to a file
	//threshholdLevel tells what threshhold to operate in, and it will continue to function until it's equal to threshholdCount
	for (; threshholdLevel < threshholdCount; threshholdLevel++) {
		//The test pointer holds where all the work will be done
		outcome::threshhold* test = head->threshholdList;
		//This loop moves the test pointer to the level that it will operate on to match threshholdLevel
		for (int i = 0; i < threshholdLevel; i++) {
			test = test->next;
		}
		//This CSV print will put the description of the threshhold at the start of each row
		writer << test->description;
		for (int f = 0; f < outcomeCount; count++) {
			//This loop will print all of the data from the outcomes with a matching threshhold
			for (outcome* current = head; current != nullptr; current = current->next) {
				if (current->id == count) {
					outcome::threshhold* out = current->threshholdList;
					//This moves the out pointer to match the threshholdLevel used previously in test
					for (int o = 0; o < threshholdLevel; o++) {
						out = out->next;
					}
					if (mode == 0) {
						writer << to_string(out->count) + "/" + to_string(studentCount);
						f++;
					}
					else if (mode == 1) {
						double percent = 0;
						double percentCount = out->count;
						double total = studentCount;
						percent = percentCount / total;
						percent = percent * 100;
						writer << to_string(percent) + "%";
						f++;
					}
				}
			}
		}
		count = 0;
	}
	//Missing work tier
	//This tier works by counting how many students there are total, and subtracting that from how many have been accounted for
	int checkCount = 0;
	count = 0;
	writer << "Missing Work";
	for (int a = 0; a < outcomeCount; count++) {
		for (outcome* current = head; current != nullptr; current = current->next) {
			if (current->id == count) {
				//This loop cycles through all the threshholds per outcome and adds to a checkCount to be used
				for (outcome::threshhold* currentThreshhold = current->threshholdList; currentThreshhold != nullptr; currentThreshhold = currentThreshhold->next) {
					checkCount = checkCount + currentThreshhold->count;
				}
				if (mode == 0) {
					writer << to_string(checkCount - studentCount) + "/" + to_string(studentCount);
					a++;
				}
				else if (mode == 1) {
					double percent = 0;
					double percentCount = checkCount - studentCount;
					double total = studentCount;
					percent = percentCount / total;
					percent = percent * 100;
					writer << to_string(percent) + "%";
					a++;
				}
				checkCount = 0;
			}
		}
	}
	writer.writeToFile("PercentCSV-" + fileCountStr + ".csv");
}

//Constructor, Destructor, and clear function; sets up the Data object, or removes all data from it

Data::Data() {
	//Initializing the head of the outcome and student lists to nullptr
	head = nullptr;
	headStudent = nullptr;
}

Data::~Data() {
	//nodePtr is used to move the node used for deleting forward after the data from each node has been deleted.
	//listNode nodePtr
	outcome* outcomePtr = head;
	students* studentPtr = headStudent;
	/*
	*While delNode (The pointer I'll use for deleting) isn't a null pointer, this will continually run down the link list,
	*pulling the delNode to the nodePtr which is heading up the list until it gets to the nullptr at the end of the list.
	*/
	//cout << "Deleting all lists...\n";
	for (outcome* delNode = head; delNode != nullptr;) {
		outcomePtr = delNode->next;
		//cout << "Deleting node: 0x" << delNode;
		//cout << ", id: " << delNode->id;
		//cout << ", title:" << delNode->title;
		//cout << ", next: 0x" << delNode->next << endl;
		delNode->next = nullptr;
		delNode->id = NULL;
		delNode->title = "";
		delNode->masteryPoints = NULL;
		delNode->pointsPossible = NULL;
		delNode->deleteThreshhold(delNode->threshholdList);
		delete delNode;
		delNode = outcomePtr;
	}
	for (students* delNode = headStudent; delNode != nullptr;) {
		studentPtr = delNode->next;
		//cout << "Deleting node: 0x" << delNode;
		//cout << ", num: " << delNode->num;
		//cout << ", score:";
		delNode->deleteScore(delNode->studentScore);
		//cout << ", next: 0x" << delNode->next << endl;
		delNode->next = nullptr;
		delNode->num = NULL;
		delete delNode;
		delNode = studentPtr;
	}
	head = nullptr;
	headStudent = nullptr;

}

void Data::clear() {
	//nodePtr is used to move the node used for deleting forward after the data from each node has been deleted.
	//listNode nodePtr
	outcome* outcomePtr = head;
	students* studentPtr = headStudent;
	/*
	*While delNode (The pointer I'll use for deleting) isn't a null pointer, this will continually run down the link list,
	*pulling the delNode to the nodePtr which is heading up the list until it gets to the nullptr at the end of the list.
	*/
	//cout << "Deleting all lists...\n";
	for (outcome* delNode = head; delNode != nullptr;) {
		outcomePtr = delNode->next;
		//cout << "Deleting node: 0x" << delNode;
		//cout << ", id: " << delNode->id;
		//cout << ", title:" << delNode->title;
		//cout << ", next: 0x" << delNode->next << endl;
		delNode->next = nullptr;
		delNode->id = NULL;
		delNode->title = "";
		delNode->masteryPoints = NULL;
		delNode->pointsPossible = NULL;
		delNode->deleteThreshhold(delNode->threshholdList);
		delete delNode;
		delNode = outcomePtr;
	}
	for (students* delNode = headStudent; delNode != nullptr;) {
		studentPtr = delNode->next;
		//cout << "Deleting node: 0x" << delNode;
		//cout << ", num: " << delNode->num;
		//cout << ", score:";
		delNode->deleteScore(delNode->studentScore);
		//cout << ", next: 0x" << delNode->next << endl;
		delNode->next = nullptr;
		delNode->num = NULL;
		delete delNode;
		delNode = studentPtr;
	}
	head = nullptr;
	headStudent = nullptr;
}

//Sub-commands of Destructor/clear; focuses on data deletion

void Data::outcome::deleteThreshhold(threshhold* threshholdList) {
	for (threshhold* delNode = threshholdList; delNode != nullptr;) {
		threshholdList = delNode->next;
		delNode->next = nullptr;
		delNode->count = NULL;
		delNode->description = "";
		delNode->points = NULL;
		delete delNode;
		delNode = threshholdList;
	}
}

void Data::students::deleteScore(score* studentScore) {
	for (score* delNode = studentScore; delNode != nullptr;) {
		studentScore = delNode->next;
		/*cout << "Deleting node: 0x" << delNode;
		cout << ", Outcome ID: " << delNode->myScore;
		cout << ", score:" << delNode->myScore;
		cout << ", next: 0x" << delNode->next << endl;*/
		delNode->next = nullptr;
		delNode->outcomeID = NULL;
		delNode->myScore = NULL;
		delete delNode;
		delNode = studentScore;
	}
}