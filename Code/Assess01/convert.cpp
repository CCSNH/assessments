#include <iostream>
#include <string>
#include <json/json.h>
#include <curl/curl.h>
#include <cstdint>
#include <memory>
#include <sstream>
using namespace std;

int convertJson(Json::Value outcomeID, int mode) {
	int conversion = 1;
	bool loop = 1;
	ostringstream stream;
	Json::Value test;
	//mode 0 is for string to int conversion
	//mode 1 is for JSON int to int conversion
	if (mode == 0) {
		while (loop == 1) {
			//test = conversion;
			stream << conversion;
			test = stream.str();
			if (test == outcomeID) {
				loop = 0;
			}
			else if (test != outcomeID) {
				conversion++;
				stream.str("");
				stream.clear();
			}
			else if (conversion > 10000) {
				//ERROR
			}
		}
	}
	else if (mode == 1) {
		while (loop == 1) {
			test = conversion;
			if (test == outcomeID) {
				loop = 0;
			}
			else if (test != outcomeID) {
				conversion++;
			}
		}
	}

	return conversion;
}