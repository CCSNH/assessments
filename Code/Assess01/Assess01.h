// CSCI285N - Capstone Project - Assesssment Team
// File: Assess01.h
// Description:  : Header file for the Assess01 application
// Author: Bill Allen
// Modified: 3-28-2020

#pragma once

/*
#ifndef __AFXWIN_H__
	#error "include 'pch.h' before including this file for PCH"
#endif
*/

#include "resource.h"		// main symbols

// CAssess01App:
// See Assess01.cpp for the implementation of this class
//

class CAssess01App : public CWinApp
{
public:
	CAssess01App();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CAssess01App theApp;
