// CSCI285N - Capstone Project - Assesssment Team
// File: Assess01Dlg.h
// Description:  : Header file for the Assess01Dlg class
// Author: Bill Allen
// Modified: 3-28-2020

#pragma once

// CAssess01Dlg dialog
class CAssess01Dlg : public CDialogEx
{
private:
	CString m_authKey;
	CString m_courseNum;

public:
	// Construction
	CAssess01Dlg(CWnd* pParent = nullptr);	// standard constructor
	virtual ~CAssess01Dlg();				// standard destructor
// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ASSESS01_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG *pMsg);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonGenReport();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

};
