#include <iostream>
#include <fstream>
#include <string>
#include "CSVWriter.h"
#include "IntList.h"
#include "time.h"
using namespace std;

int main()
{
	
	// Do "Press any key to continue..." on exit
	atexit([] {system("pause"); });
	
	int studNum;					//the number of students
	int outNum;						//the number of outcomes
	const int studentID = 77832;	// Constant for Student ID
	const int outcomeID = 54321;	// Constant for Outcome ID
	string out;						//Name of Outcome

	Data class1;
	CSVWriter operations;
	
	int score;						// student's random score
	srand(time(0));					//Gets random numbers
	
	cout << "How many students? >> ";
	cin >> studNum;
	
	cout << "How many outcomes? >> ";
	cin >> outNum;

	for (int i = 0; i < studNum; i++)
	{
		class1.addStudent(i + studentID);
	}
	
	for (int j = 0; j < outNum; j++)
	{
		for (int i = 0; i < studNum; i++)
		{
			score = rand() % 6;
			class1.giveScore(i + studentID, outcomeID + j, score);
		}
	}

	for (int i = 0; outNum > i; i++)
	{
		cout << "Please enter the name of the outcome you want to add: ";
		cin >> out;


		class1.addOutcome(i + outcomeID, out);

	}
	cout << endl; //Formatting

	class1.checkOutcome();
	class1.checkStudent();
	
	
}