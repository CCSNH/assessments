# Assessments

* Allen, William C.
* Brancheau, Logan A.
* Morvant, Adam T.
* Psareas, Nicholas

The documentation for this project can be found in this directory as the file "CSCI285N_Assessment_User_Documentation_v1.1.1.pdf".

The setup file for the project can be found in this repository's root directory as PARGSetup.exe.

The code for this project can be found inside of the "Code" folder, and is split between three folders.
	-"Assess01" is the final version of the project, and includes all of the features as well as a GUI.
	-"CSV Code" is the code used in production of generating CSV files.
	-"ProjTest" is the original version of the project that does not run with a GUI. Note that this version of the code is old and deprecated.